import React, { Fragment } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Products from "./components/products/Products";
import Cart from "./components/cart/Cart";

function App() {
  return (
    <Fragment>
        <Router>
          <Switch>
            <Route exact path="/" component={Products} />
            <Route exact path="/cart" component={Cart} />
          </Switch>
        </Router>
    </Fragment>
  );
}

export default App;
