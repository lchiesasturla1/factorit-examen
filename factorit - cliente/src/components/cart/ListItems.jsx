import React, {useState,useEffect,Fragment} from 'react';
import styled from 'styled-components'

import Item from './Item'
import Modal from './Modal';

const ContainerItems = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Button = styled.a`
    width: 300px;
    margin: 20px 0 20px 0;
    color: white !important;
`;

const Total = styled.h2`
    margin-top: 30px;
`;

const ListItems = () => {

    
    const [total, setTotal] = useState(0);

    useEffect(() =>{
        setTimeout(() => {
            calculateTotal();
      }, 50);
    });

    const calculateTotal = () =>{
        let elements = document.getElementsByTagName("h6");
        let elementPrice = 0;
        let totalPrice = 0;
        let lowestPrice = 0;
        for(let i = 0 ; i < elements.length ; i++){
            elementPrice = parseInt(elements[i].innerHTML.substring(2,elements[i].length));
            if(lowestPrice !=0){
                if( elementPrice < lowestPrice){
                    lowestPrice = elementPrice;
                }
            }else{
                lowestPrice = elementPrice;
            }
            totalPrice += elementPrice;
        }
        let discount = getDiscount(totalPrice,lowestPrice);
        generateProductArray();
        setTotal(totalPrice - discount);

        sessionStorage.setItem('total', total);
    }

    function getDiscount (total,lowestPrice){
        let keys = Object.keys(localStorage);
        let itemsCount = 0;
        let modifier = 0;
        let rol = sessionStorage.getItem('rol');
        keys.map((key) =>{
            itemsCount += parseInt(localStorage.getItem(key));
        })

        if(itemsCount === 4){
            modifier = total * 0.25;
        }else if(itemsCount > 10){
            if(rol == 0){
                modifier = 100;
            }else{
                modifier = 500 + lowestPrice;
            }
        }
        return modifier;
    }

    const generateProductArray = () => {
        let keys = Object.keys(localStorage);
        let items = [];
        keys.map((key) =>{
            items.push({"idProduct": parseInt(key),
                        "count": parseInt(localStorage.getItem(key))
            });
        })
        sessionStorage.setItem('items', JSON.stringify(items));
    }

    const removeAllItems = () =>{
        localStorage.clear();

    }

    return ( 
        <ContainerItems>
            <h1>Carrito {sessionStorage.getItem('rol') == 1 ?<span className="badge badge-success">VIP</span> : null}</h1>
            {Object.keys(localStorage).map((key) => (
                <Item 
                    key = {key}
                    id = {key}
                />
            ))}
            <Total id = "total">Total: ${total}</Total>
            {localStorage.length === 0
            ?
            <Fragment>
                <Button data-toggle="modal" data-target="#ModalPurchase" className="btn btn-success disabled">Realizar pedido</Button>
                <Button href="/" className="btn btn-danger disabled" onClick={() => {removeAllItems();}}>Eliminar carrito</Button>
            </Fragment>
            :
            <Fragment>
                <Button data-toggle="modal" data-target="#ModalPurchase" className="btn btn-success">Realizar pedido</Button>
                <Button href="/" className="btn btn-danger" onClick={() => {removeAllItems();}}>Eliminar carrito</Button>
            </Fragment>
            }
            <Modal
                total = {total}
            />
        </ContainerItems>
     );
}
 
export default ListItems;