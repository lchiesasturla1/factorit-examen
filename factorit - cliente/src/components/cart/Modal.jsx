import React, {useState, Fragment} from 'react';
import styled from 'styled-components'
import { useHistory } from "react-router-dom";

import Confirm from '../../img/confirm.png'

const ConfirmContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

const ConfirmImage = styled.img`
    width: 30px;
`;

const Modal = ({total}) => {

    const [confirm,setConfirm] = useState(false);

    let history = useHistory();
    const doPurchase = async () =>{
        setConfirm(false);
        fetch('http://localhost:8080/cart/', {
            method: 'POST',
            headers: { 
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                total: sessionStorage.getItem('total'),
                username: sessionStorage.getItem('user'),
                productsAsociated: JSON.parse(sessionStorage.getItem('items'))
            })
        }).catch(error => console.error(error));
        setConfirm(true);
        localStorage.clear();
        sessionStorage.clear();
        setTimeout(() => {
            document.getElementsByClassName('modal-backdrop')[0].setAttribute("style","display : none;")
            history.push("/")
        }, 3000);
        
    }

    
    return ( 
        <div className="modal fade" id="ModalPurchase" tabIndex="-1" role="dialog" aria-labelledby="LabelModal" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                {confirm 
                
                ? 
                <Fragment>
                    <div className="modal-header">
                        <h5 className="modal-title" id="LabelModal">Compra confirmada</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <ConfirmContainer className="modal-body">
                        <ConfirmImage src={Confirm} alt="Confirmacion"/>
                        <p className = "mt-3">Se ha confirmado la compra.</p>
                    </ConfirmContainer>
                </Fragment>
               
                :
                <Fragment>
                    <div className="modal-header">
                        <h5 className="modal-title" id="LabelModal">Confirmacion de la compra</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        Desea confirmar la compra? El total es: {total}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-success" onClick={() => {doPurchase();}}>Confirmar</button>
                    </div>
                </Fragment>
                }
                
                </div>
            </div>
        </div>
     );
}
 
export default Modal;