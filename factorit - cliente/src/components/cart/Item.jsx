import React, {useState, useEffect, Fragment} from "react";
import styled from "styled-components";


const ContainerItem = styled.div`
  width: 90%;
  height: 100px;
  border: 1px lightgray solid;
  border-radius: 15px;
  margin-top: 25px;
  box-shadow: 0px 1px 3px #d9d9d9;
`;

const Name = styled.h2`
  font-family: Raleway;
  font-weight: 600;
  color: #7000ff;
  font-size: 28px;
  margin: 10px 0 0 10px;
  display: inline-block;
`;

const Price = styled.h6`
  display: inline-block;
  float: right;
  margin: 10px 10px 0 0;
  font-style: italic;
  font-family: Raleway;
  font-size: 20px;
`;

const Units = styled.div`
  display: flex;
  margin: 25px 0 0 10px;
`;

const Quantity = styled.p`
  font-size: 20px;
  margin: 0 7px 0 7px;
  font-family: Raleway;
  line-height: 18px;
`;




const Item = ({id}) => {

    const [count , setCount] = useState(parseInt(localStorage.getItem(id), 10));
    const [item, setItem] = useState({})
    useEffect(() =>{
      
      getProductById(id);
    },[])

    const getProductById = async (id) =>{

      const url = `http://localhost:8080/products/${id}`;
      const request = await fetch(url);
      const response = await request.json();
      setItem(response);
    }

  return (
    <Fragment>
        {count !== 0 
        ? 
        
            <ContainerItem>
            <Name>{item.name}</Name>
            <Price>{`$ ${count * item.price}`} </Price>
            
            
            <Units>
                <Quantity>Unidades: {count}</Quantity>
            </Units>

            </ContainerItem>
        
        :
        null
        }
    </Fragment>
      
    
  );
};

export default Item;
