import React, { Fragment, useEffect } from "react";
import Header from "../common/Header";
import ListItems from "./ListItems";

const Cart = () => {

  useEffect(() =>{
    sessionStorage.setItem('stage','cart')
  })

  return (
    <Fragment>
      <Header />
      <ListItems />
    </Fragment>
  );
};

export default Cart;
