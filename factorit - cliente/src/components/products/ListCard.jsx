import React, {useState, useEffect} from "react";
import styled from "styled-components";

import Card from "./Card";

const Column = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 20px;
`;




const ListCard = () => {

  const [products, setProducts] = useState([]);

  useEffect(() => {

    getProducts();

  }, [])

  const getProducts = async () =>{

    const url = `http://localhost:8080/products/`;
    const request = await fetch(url);
    const response = await request.json();
    setProducts(response);
  }

  return (
    <div className="container">
      <div className="row">
        {products.map((product) => (
            <Column className="col-12 col-sm-6 col-md-4 col-lg-3">
                <Card
                    key={product.idProduct}
                    id={product.idProduct}
                    name={product.name}
                    price={product.price}
                />
            </Column>
            ))}
      </div>
    </div>
  );
};

export default ListCard;
