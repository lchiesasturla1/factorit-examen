import React, {useState,useEffect} from "react";
import styled from "styled-components";

const ContainerCard = styled.div`
  width: 90%;
  height: 300px;
  border: 1px lightgray solid;
  border-radius: 15px;
  box-shadow: 0px 2px 3px rgb(0,0,0,0.3);
  &:hover{
      cursor: pointer;
  }
`;

const Image = styled.div`
  height: 150px;
  border-bottom: 1px lightgray solid;
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
`;

const BodyCard = styled.div`
    height: 100px;
`;

const TitleCard = styled.h3`
  font-family: Raleway;
  font-size: 24px;
  font-weight: 600;
  margin-top: 10px;
  padding-left: 10px;
`;



const FooterCard = styled.div`
  display: flex;
  height: 30px;
  padding-top: 7px;
  border-top: 1px lightgray solid;
`;

const Price = styled.p`
  margin-bottom: 0;
  margin-left: 10px;
  font-family: Raleway;
  font-weight: 600;
  color: #7000ff;
`;

const Button = styled.div`
  border-style: none;
  width: 20px;
  height: 20px;
  border-radius: 60px;
  background-color: #7000ff;
  color: white;
  justify-content: center;
  align-items: center;
  display: flex;
`;

const Quantity = styled.p`
  font-size: 20px;
  margin: 0 7px 0 7px;
  font-family: Raleway;
  line-height: 18px;
`;

const Card = ({ id, name, price }) => {

    const [count,setCount] = useState(0);
    const [itemsCharged,setItemsCharged] = useState(false);

    useEffect(() => {
      if(!itemsCharged){
        setCount(0);
        if(localStorage.getItem(id)){
          setCount(localStorage.getItem(id));
        }else{
          setCount(0);
        }

        setItemsCharged(true);
      }
    },[itemsCharged, id]);

    const addItem = id =>{
        
        setCount(parseInt(count) + 1);
        let item = localStorage.getItem(id);
        if(item !== null){
          localStorage.removeItem(id);
        }
        setCount(parseInt(count) + 1);
        localStorage.setItem(id, parseInt(count) + 1);
    }

    const removeItem = id =>{
        
        if(count !== 0){
          setCount(parseInt(count) - 1);
          localStorage.setItem(id, parseInt(count) - 1);
        }

        if(count === 1)
          localStorage.removeItem(id);
    }

  return (
    <ContainerCard>
      <Image></Image>
      <BodyCard>
        <TitleCard>{name}</TitleCard>
      </BodyCard>

      <FooterCard>
        <Price>${price}</Price>
        <Button className="ml-auto" onClick={() => {removeItem(id);}}>-</Button>
        <Quantity>{count}</Quantity>
        <Button className="mr-2" onClick={() => {addItem(id);}}>+</Button>
      </FooterCard>
    </ContainerCard>
  );
};

export default Card;
