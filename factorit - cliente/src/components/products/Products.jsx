import React, {Fragment, useEffect} from 'react';
import Header from "../common/Header";
import ListCard from "./ListCard";

const Products = () => {
    useEffect(() =>{
        sessionStorage.setItem('stage','products')
    })
    return ( 
        <Fragment>
            <Header/>
            <ListCard/>
        </Fragment>
     );
}
 
export default Products;