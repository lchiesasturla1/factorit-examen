import React, {useState,useEffect} from "react";
import styled from "styled-components";

const Title = styled.a`
  color: #7000ff !important;
  font-family: Raleway;
  font-weight: 800;
`;

const NavOptions = styled.ul`
  flex-direction: row;
`;

const Option = styled.a`
  margin-right: 20px;
  color: #7000ff;
  font-weight: 500;
  transition: 0.3s;
  &:hover {
    color: #9916ff;
    text-decoration: none;
  }
`;

const Label = styled.label`
    color: #7000FF;
    font-weight: 500;
    margin-right: 20px;
`;

const Header = () => {

    const [stage, setStage] = useState('products')

    useEffect( () => {
        let rol = document.getElementById('switchRol');
        let actualRol = sessionStorage.getItem('rol');
        setTimeout(() => {
          setStage(sessionStorage.getItem('stage'));
        }, 10);
        
        if(actualRol){
            if(stage == 'products'){
                if(actualRol == 1){
                    rol.checked = true;
                }else{
                    rol.checked = false;
                }
            }

                
        }else{
            sessionStorage.setItem('rol', 0);
            sessionStorage.setItem('user', 'comun');
        }
    },[]);


    const setRol = () => {
        sessionStorage.removeItem('rol');
        let rol = document.getElementById('switchRol');

        if(rol.checked === true){
            sessionStorage.setItem('rol' , 1);
            sessionStorage.setItem('user', 'vip');
        }else{
            sessionStorage.setItem('rol' , 0);
            sessionStorage.setItem('user', 'comun');
        }

    }

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-transparent">
      <strong>
        <Title className="navbar-brand" href="/">
          FactorIT
        </Title>
      </strong>

      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <NavOptions className="navbar-nav ml-auto">
          {stage == 'products' 
        
          ? 
          <li className="nav-item">
              <div className="form-check">
              <input className="form-check-input" type="checkbox" value="" id="switchRol" onClick={() => {setRol();}}/>
                  <Label className="form-check-label" htmlFor="switchRol">
                      VIP
                  </Label>
              </div>
          </li>
          :
          null
          }
          <li className="nav-item">
            <Option href="/">PRODUCTOS</Option>
          </li>
          <li className="nav-item">
            <Option href="/cart">CARRITO</Option>
          </li>
        </NavOptions>
      </div>
    </nav>
  );
};

export default Header;
