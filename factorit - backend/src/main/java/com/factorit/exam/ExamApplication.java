package com.factorit.exam;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import com.factorit.models.Product;
import com.factorit.repository.ProductRepository;

@ComponentScan(basePackages = {"com.factorit.controller"})
@EntityScan("com.factorit.models")
@EnableJpaRepositories("com.factorit.repository")
@SpringBootApplication
public class ExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamApplication.class, args);
	}

	@Bean
    public CommandLineRunner insertSampleProducts(ProductRepository productRepository) {
        return args -> {
            List<Product> sampleProducts = new ArrayList();
            
            sampleProducts.add(new Product("Placa de video Nvidia - RTX 2060", 50000));
            sampleProducts.add(new Product("Teclado Logitech G230", 1430));
            sampleProducts.add(new Product("Escritorio blanco de melamina", 3580));
            sampleProducts.add(new Product("Software a medida de factorIT ;)", 100000));
            sampleProducts.add(new Product("Notebook Lenovo T430", 25000));
            sampleProducts.add(new Product("Mouse Genius 960", 1000));
            sampleProducts.add(new Product("Sillon Ejecutivo - Negro", 10000));
            sampleProducts.add(new Product("Auriculares Bluetooth", 5000));
            

            for (int i = 0; i < sampleProducts.size(); i++) {
                productRepository.save(sampleProducts.get(i));
                System.out.println("\nSe inserto el siguiente producto:\n");
                System.out.println("ID del producto: " + sampleProducts.get(i).getIdProduct());
                System.out.println("Nombre del producto: " + sampleProducts.get(i).getName());
                System.out.println("Precio: " + sampleProducts.get(i).getPrice());
            }

           
        };
    }
	
}
