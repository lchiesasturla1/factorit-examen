package com.factorit.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.factorit.models.CartDetails;

public interface CartDetailsRepository extends CrudRepository<CartDetails,Long>{

	@Query("Select cd from CartDetails cd where cd.cart.idCart = :idCart ")
    List<CartDetails> findProductsByCart(@Param("idCart") Long idCart);
}
