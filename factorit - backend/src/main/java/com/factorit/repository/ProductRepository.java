package com.factorit.repository;

import org.springframework.data.repository.CrudRepository;

import com.factorit.models.Product;

public interface ProductRepository extends CrudRepository<Product,Long>{

}
