package com.factorit.repository;

import org.springframework.data.repository.CrudRepository;

import com.factorit.models.Cart;

public interface CartRepository extends CrudRepository<Cart,Long>{

}
