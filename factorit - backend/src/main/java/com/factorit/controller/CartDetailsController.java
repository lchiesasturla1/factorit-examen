package com.factorit.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.factorit.models.CartDetails;
import com.factorit.models.Product;
import com.factorit.pojo.ResponseCartDetails;
import com.factorit.repository.CartDetailsRepository;
import com.factorit.repository.ProductRepository;

@RestController
@RequestMapping("/cartdetails")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class CartDetailsController {
	
	@Autowired
	CartDetailsRepository cartDetailsRepository;
	
	@Autowired
	ProductRepository productRepository;
	
    @GetMapping("/{id}")
    public List<ResponseCartDetails> findProductsByCart(@PathVariable(value = "id") Long idCart) {
    	List<ResponseCartDetails> response = new ArrayList();
        List<CartDetails> products = cartDetailsRepository.findProductsByCart(idCart);
        for(int i = 0 ; i < products.size() ; i++) {
        	Optional<Product> optProduct = productRepository.findById(new Long(products.get(i).getIdProduct()));
        	Product product = optProduct.get();
        	response.add(new ResponseCartDetails(product.getIdProduct(),product.getName(),products.get(i).getCount(),products.get(i).getCount() * product.getPrice()));
        }
        
        return response;
    }
	
	
}
