package com.factorit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.factorit.repository.ProductRepository;
import com.factorit.models.Cart;
import com.factorit.models.Product;

@RestController
@CrossOrigin(origins = "*" , methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/products")
public class ProductController {

	@Autowired
	ProductRepository productRepository;
	
    @GetMapping("/")
    public List<Product> findAll() {
        List<Product> response = (List<Product>) productRepository.findAll();

        return response;
    }
	
    @GetMapping("/{id}")
    public Optional<Product> findProductById(@PathVariable(value = "id") Long idProduct) {
        Optional<Product> product = productRepository.findById(idProduct);
        return product;
    }

    @PostMapping("/")
    public Product addProduct(@RequestBody Product product) {
        productRepository.save(product);

        return product;
    }
	
    @PutMapping("/{id}")
    public Product modifyProduct(@PathVariable(value = "id") Long idProduct,@RequestBody Product productModify) {
        Optional<Product> product = productRepository.findById(idProduct);
        Product productUpdate = product.get();
        if(productModify.getName() != "")
        	productUpdate.setName(productModify.getName());
        
        if(productModify.getPrice() != 0)
        	productUpdate.setPrice(productModify.getPrice());
        
        productRepository.save(productUpdate);
        
        return productUpdate;
    }
    
    @DeleteMapping("/{id}")
    public HashMap<String,String> modifyProduct(@PathVariable(value = "id") Long idProduct) {
    	HashMap<String,String> response = new HashMap();
    	try {
        	Optional<Product> product = productRepository.findById(idProduct);
        	response.put("id", String.valueOf(idProduct));
        	if(product.isPresent()) {
        		productRepository.deleteById(idProduct);
        		response.put("state", "deleted");
        	}else {
        		response.put("state", "doesn't exist");
        	}
    	}catch(Exception e) {
    		response.put("error",e.getMessage());
    	}

    	return response;
    }
    
}
