package com.factorit.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.factorit.models.Cart;
import com.factorit.models.CartDetails;
import com.factorit.repository.CartDetailsRepository;
import com.factorit.repository.CartRepository;

@RestController
@RequestMapping("/cart")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class CartController {

	@Autowired
	CartRepository cartRepository;
	
	@Autowired
	CartDetailsRepository cartDetailsRepository;
	
	
	@PostMapping("/")
	public Cart addCart(@RequestBody Cart cart) {
		List<CartDetails> listDetails = new ArrayList<CartDetails>();
		cartRepository.save(cart);
		for(int i = 0 ; i < cart.getProductsAsociated().size() ; i++) {

			CartDetails cartDetails = new CartDetails(
					 cart.getProductsAsociated().get(i).getIdProduct(),
					 cart.getProductsAsociated().get(i).getCount(),
					 cart);
			listDetails.add(cartDetails);
			cartDetailsRepository.save(cartDetails);
												
		}
		cart.setProductsAsociated(listDetails);
		return cart;
	}
	
	@GetMapping("/{id}")
	public Cart getCartById(@PathVariable(value = "id") Long idCart) {
		Optional<Cart> cart = cartRepository.findById(idCart);
		return cart.get();
	}
}
