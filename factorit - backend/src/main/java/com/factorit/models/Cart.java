package com.factorit.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cart") 
public class Cart {
	
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY ) 
	private Long idCart;
	
	@Column(name="username", length= 45,nullable = false)
	private String username;
	
	@Column(name="total", nullable = false)
	private int total;
	
	@OneToMany(mappedBy="cart")
	private List<CartDetails> productsAsociated;

	
	
	public Cart() {
		super();
	}

	public Long getIdCart() {
		return idCart;
	}

	public void setIdCart(Long idCart) {
		this.idCart = idCart;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<CartDetails> getProductsAsociated() {
		return productsAsociated;
	}

	public void setProductsAsociated(List<CartDetails> productsAsociated) {
		this.productsAsociated = productsAsociated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCart == null) ? 0 : idCart.hashCode());
		result = prime * result + ((productsAsociated == null) ? 0 : productsAsociated.hashCode());
		result = prime * result + total;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cart other = (Cart) obj;
		if (idCart == null) {
			if (other.idCart != null)
				return false;
		} else if (!idCart.equals(other.idCart))
			return false;
		if (productsAsociated == null) {
			if (other.productsAsociated != null)
				return false;
		} else if (!productsAsociated.equals(other.productsAsociated))
			return false;
		if (total != other.total)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
