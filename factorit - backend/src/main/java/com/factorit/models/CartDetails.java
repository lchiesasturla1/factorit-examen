package com.factorit.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "cartdetails") 
public class CartDetails {
	
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY )
	private Long idDetail;
	
	@Column(name="idProduct", nullable = false)
	private int idProduct;
	
	@Column(name="count", nullable = false)
	private int count;
	
	@ManyToOne
	@JoinColumn(name="idCart")
	private Cart cart;
	
	
	
	public CartDetails() {
		super();
	}

	public CartDetails(int idProduct, int count, Cart cart) {
		super();
		this.idProduct = idProduct;
		this.count = count;
		this.cart = cart;
	}



	public Long getIdDetail() {
		return idDetail;
	}

	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idCart")
	@JsonIdentityReference(alwaysAsId = true) 
	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cart == null) ? 0 : cart.hashCode());
		result = prime * result + count;
		result = prime * result + ((idDetail == null) ? 0 : idDetail.hashCode());
		result = prime * result + idProduct;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartDetails other = (CartDetails) obj;
		if (cart == null) {
			if (other.cart != null)
				return false;
		} else if (!cart.equals(other.cart))
			return false;
		if (count != other.count)
			return false;
		if (idDetail == null) {
			if (other.idDetail != null)
				return false;
		} else if (!idDetail.equals(other.idDetail))
			return false;
		if (idProduct != other.idProduct)
			return false;
		return true;
	}

	
	
}
