Examen Tecnico FactorIT - Lucas Chiesa

Cuenta con 4 partes:

Frontend en React con Bootstrap/Styled Components.
Backend Spring Boot 2.3.2 con JPA y Hibernate.
bd (Base de datos) MySQL.
Proyecto postman con los endpoints expuestos.

Proceso de ejecucion:

Primero se debe ejecutar el script incluido en la carpeta "bd" con el nombre **createdatabase.sql** sobre una base de datos MySQL. Este creara la base de datos en la que luego la aplicacion creara las tablas.

Posteriormente de ello se debe deployar el proyecto maven incluido en la carpeta "backend" pero primero se deben modificar ciertas configuraciones de conexion en el archivo "application.properties".


1. Se debera modificar la url con la ip y puerto que manejen ustedes, en este caso por default va localhost y 3306.
2. Se debera modificar el usuario y contraseña con el suyo propio.

Una vez hecho esto, ya se puede deployar la aplicacion con maven goals de **spring-boot:run**.

Ya terminado esto y viendo que el servidor levanto correctamente se puede proseguir a levantar el proyecto react ubicado en la carpeta "frontend", en este probablemente se le tenga que hacer un **npm install** para que genere su carpeta de node_modules propia.

Una vez creado esto, se puede deployar el proyecto frontend con el comando **npm start** y ya se podria probar la aplicacion correctamente.

Aunque la aplicacion usa pocos endpoints, dejo todos los endpoints que deje creados:

- GET /products/ **Trae todos los productos**
- GET /products/{idProduct} **Trae el producto por id**
- POST /products/ **Crea un producto en la base de datos**
- PUT /products/{idProduct} **Modifica un producto del id correspondiente**
- DELETE /products/{idProduct} **Elimina un producto**
- GET /cartdetails/{idCart} **Te trae los productos comprados en ese carrito**
- GET /cart/{idCart} **Te trae la informacion de ese carrito**
- POST /cart/ **Crea un carrito**

Todos estos endpoints tienen un ejemplo en el proyecto de postman.

Un saludo y cualquier cosa que haya surgido, estoy completamente a su disposicion.